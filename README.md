Getting started
-
____

- Create ssl certificate

  ```openssl req -x509 -newkey rsa:4096 -keyout serverkey.pem -out servercert.pem -days 365 -nodes```

- Build and run the server

  `go build && ./server`