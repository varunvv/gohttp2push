package main

import (
	"fmt"
	"net/http"
	"time"
)

func closeNotify(w http.ResponseWriter, r *http.Request) {
	rc := make(chan struct{})
	if nw, ok := w.(http.CloseNotifier); ok {
		go func() {
			for {
				<-nw.CloseNotify()
				rc <- struct{}{}
			}
		}()
	}
	select {
	case <-time.NewTicker(3 * time.Second).C:
		fmt.Println("Client stayed")
		w.Write([]byte("OK"))
	case <-rc:
		fmt.Println("Client left")
	}
}

func hijack(w http.ResponseWriter, r *http.Request) {
	if nw, ok := w.(http.Hijacker); ok {
		if conn, buf, err := nw.Hijack(); err != nil {
			fmt.Println("Error hijacking connection", err)
			w.Write([]byte("Error"))
		} else {
			buf.WriteString("Hello\n")
			buf.Flush()
			conn.Close()
		}
		return
	}
	fmt.Println("Couldn't hijack connection.")
	w.Write([]byte("OK"))
}

func flush(w http.ResponseWriter, r *http.Request) {
	if fw, ok := w.(http.Flusher); ok {
		for i := 0; i < 10; i++ {
			fmt.Fprintf(w, "Chunk %d\n", i+1)
			time.Sleep(time.Second)
			fw.Flush()
		}
		return
	}
	fmt.Println("Not a flusher.")
	w.Write([]byte("OK"))
}

func image(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/png")
	http.ServeFile(w, r, "./http2-logo.png")
}

func root(w http.ResponseWriter, r *http.Request) {
	if p, ok := w.(http.Pusher); ok {
		p.Push("/image", nil)
	} else {
		fmt.Println("Not a pusher")
	}
	w.Header().Add("Content-Type", "text/html")
	http.ServeFile(w, r, "./index.html")
}

func main() {
	fmt.Println("Starting server", time.Now())
	http.HandleFunc("/closenotify", closeNotify)
	http.HandleFunc("/hijack", hijack)
	http.HandleFunc("/flush", flush)
	http.HandleFunc("/", root)
	http.HandleFunc("/image", image)
	http.ListenAndServeTLS(":8080", "./servercert.pem", "./serverkey.pem", nil)
}
